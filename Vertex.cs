﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework10
{
    public class Vertex<T>: INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }
        public int EdgesWeightSum { get; set; }
        public bool IsUnvisited { get; set; }
        public INode<T> PreviousVertex { get; set; }

        public Vertex(int id, T data)
        {
            Id = id;
            Data = data;
            EdgesWeightSum = int.MaxValue;
            IsUnvisited = true;
            PreviousVertex = default(INode<T>);
        }

        public override string ToString()
        {
            return $"Id: {Id} , данные: {Data.ToString()}";
        }
    }
}
