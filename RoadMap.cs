﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework10
{
    class RoadMap<V, T> : Graph<V, T> where V : INode<T>
    {
        public RoadMap(IEnumerable<V> vertices, int[,] matrix) : base(vertices, matrix)
        {
        }

        /// <summary>
        /// Функция поиска пути
        /// </summary>
        /// <param name="startCity">Стартовый город</param>
        /// <param name="endCity">Конечный город</param>
        /// <returns></returns>
        public string FindPart(T startCity, T endCity)
        {
            return FindShortestPath(GetVertexByData(startCity), GetVertexByData(endCity));
        }

        /// <summary>
        /// Поиск кратчайшего пути по вершинам
        /// </summary>
        /// <param name="startVertex">Стартовая вершина</param>
        /// <param name="finishVertex">Финишная вершина</param>
        /// <returns>Кратчайший путь</returns>
        private string FindShortestPath(V startVertex, V finishVertex)
        {
            var first = startVertex;
            first.EdgesWeightSum = 0;
            while (true)
            {
                var current = FindUnvisitedVertexWithMinSum();
                if (current == null)
                {
                    break;
                }

                SetSumToNextVertex(current);
            }

            return GetPathInfo(startVertex, finishVertex);
        }

        /// <summary>
        /// Поиск непосещенной вершины с минимальным значением суммы
        /// </summary>
        /// <returns>Информация о вершине</returns>
        private V FindUnvisitedVertexWithMinSum()
        {
            var minValue = int.MaxValue;
            V minVertexInfo = default(V);
            foreach (var vertex in Vertices.Values)
            {
                if (vertex.IsUnvisited && vertex.EdgesWeightSum < minValue)
                {
                    minVertexInfo = vertex;
                    minValue = vertex.EdgesWeightSum;
                }
            }

            return minVertexInfo;
        }

        /// <summary>
        /// Вычисление суммы весов ребер для следующей вершины
        /// </summary>
        /// <param name="vertex">Информация о текущей вершине</param>
        private void SetSumToNextVertex(V vertex)
        {
            vertex.IsUnvisited = false;
            foreach (var edge in Edges.Where(x => x.From == vertex.Id))
            {
                var nextVertex = GetVertex(edge.To);
                var sum = vertex.EdgesWeightSum + edge.Weight;
                if (sum < nextVertex.EdgesWeightSum)
                {
                    nextVertex.EdgesWeightSum = sum;
                    nextVertex.PreviousVertex = vertex;
                }
            }
        }

        /// <summary>
        /// Формирование пути
        /// </summary>
        /// <param name="startVertex">Начальная вершина</param>
        /// <param name="endVertex">Конечная вершина</param>
        /// <returns>Путь</returns>
        private string GetPathInfo(V startVertex, V endVertex)
        {
            List<string> pathList = new List<string>();

            int fullDistance = 0;
            var fullPath = endVertex.Data.ToString();
            while (startVertex.Id != endVertex.Id)
            {
                var currDictance = Edges.First(x => x.To == endVertex.Id && x.From == endVertex.PreviousVertex.Id).Weight;
                fullDistance += currDictance;
                pathList.Add($"{endVertex.PreviousVertex.Data} -> {endVertex.Data} : {currDictance}");

                endVertex = (V)endVertex.PreviousVertex;
                fullPath = endVertex.Data + " -> " + fullPath;
            }
            pathList.Reverse();
            pathList.Add($"Весь маршрут: {fullPath}, общая дистанция: {fullDistance}");

            return String.Join(Environment.NewLine, pathList);
        }
    }
}
