﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework10
{
    class Program
    {
        static void Main(string[] args)
        {
            // список вершин
            List<Vertex<string>> vertices = new List<Vertex<string>>
            {
                new Vertex<string>(1,"Москва"),
                new Vertex<string>(2,"Уфа"),
                new Vertex<string>(3,"Новосибирск"),
                new Vertex<string>(4,"Казань"),
                new Vertex<string>(5,"Барнаул"),
                new Vertex<string>(6,"Хабаровск"),
                new Vertex<string>(7,"Волгоград")
            };

            // матрица смежности
            int[,] matrix = new int[7, 7]
            {
                {   0,  300,    0, 400,   0,    0, 500 },
                { 300,    0, 1000, 200,   0,    0,   0 },
                {   0, 1000,    0,   0, 250, 3000,   0 },
                { 400,  200,    0,   0,   0,    0, 350 },
                {   0,    0,  250,   0,   0,    0,   0 },
                {   0,    0, 3000,   0,   0,    0,   0 },
                { 500,    0,    0, 350,   0,    0,   0 }
            };

            var roadMap = new RoadMap<Vertex<string>, string>(vertices, matrix);
            //roadMap.Remove(7);
            //roadMap.ShowMatrixByConsole();

            Console.Write("\nВведите город начала маршрута: ");
            string startCity = Console.ReadLine();
            Console.Write("Введите город конца маршрута: ");
            string endCity = Console.ReadLine();

            if (!roadMap.Contains(startCity))
            {
                Console.WriteLine($"Город {startCity} отсутствует в списке доступных");
            }
            else if (!roadMap.Contains(endCity))
            {
                Console.WriteLine($"Город {endCity} отсутствует в списке доступных");
            }
            else
            {                
                Console.WriteLine($"Маршрут от {startCity} до {endCity}");
                Console.WriteLine(roadMap.FindPart(startCity, endCity));
            }
            Console.ReadKey();
        }
    }
}
