﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework10
{
    public class Graph<V, T> where V : INode<T>
    {
        public readonly Dictionary<int, V> Vertices = new Dictionary<int, V>();
        public readonly List<Edge<V, T>> Edges = new List<Edge<V, T>>();

        public void Add(V vertex)
        {
            Vertices.Add(vertex.Id, vertex);
        }

        public void Remove(int id)
        {
            Vertices.Remove(id);
            Edges.RemoveAll(x => x.To == id || x.From == id);
        }

        public void Remove(V vertex)
        {
            Remove(vertex.Id);
        }

        public bool Contains(int id)
        {
            return Vertices.ContainsKey(id);
        }

        public bool Contains(T Data)
        {
            return Vertices.Values.Any(x => x.Data.Equals(Data));
        }

        public bool Contains(V vertex)
        {
            return Contains(vertex.Id);
        }

        public T FindData(int id)
        {
            Vertices.TryGetValue(id, out var vertex);
            return vertex.Data;
        }

        public V GetVertexByData(T Data)
        {
            return Vertices.Values.FirstOrDefault(x => x.Data.Equals(Data));
        }

        public V GetVertex(int id)
        {
            Vertices.TryGetValue(id, out var vertex);
            return vertex;
        }

        public int VerticesCount()
        {
            return Vertices.Count;
        }

        /// <summary>
        /// Инициализация графа с помощью списка вершин и матрицы смежности
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="matrix"></param>
        public Graph(IEnumerable<V> vertices, int[,] matrix)
        {
            foreach (var vertex in vertices)
            {
                Vertices.Add(vertex.Id, vertex);
            }

            for (int i = 0; i < Vertices.Count; i++)
            {
                for (int j = 0; j < Vertices.Count; j++)
                {
                    if (!Vertices.TryGetValue(i + 1, out var vertex1))
                        throw new Exception("Вершина не проинициализирована");
                    if (!Vertices.TryGetValue(j + 1, out var vertex2))
                        throw new Exception("Вершина не проинициализирована");
                    if (matrix[i, j] != 0)
                        Edges.Add(new Edge<V, T>(vertex1, vertex2, matrix[i, j]));
                }
            }
        }

        /// <summary>
        /// Получить матрицу смежности
        /// </summary>
        /// <returns>Матрица смежности</returns>
        private int[,] GetMatrix()
        {
            var matrix = new int[Vertices.Count, Vertices.Count];

            foreach (var edge in Edges)
            {
                var row = edge.From - 1;
                var column = edge.To - 1;

                matrix[row, column] = edge.Weight;
            }

            return matrix;

        }

        /// <summary>
        /// Вывести матрицу смежности на консоль
        /// </summary>
        public void ShowMatrixByConsole()
        {
            var matrix = GetMatrix();

            //Шапка
            Console.Write("X||");
            for (int i = 0; i < Vertices.Count; i++)
            {
                Console.Write(Vertices[i + 1].Data + "|");
            }
            Console.WriteLine();

            //Таблица
            for (int i = 0; i < Vertices.Count; i++)
            {
                Console.Write(Vertices[i + 1].Data + "||");
                for (int j = 0; j < Vertices.Count; j++)
                {
                    Console.Write(matrix[i, j] + "|");
                }
                Console.WriteLine();
            }
        }
    }
}
