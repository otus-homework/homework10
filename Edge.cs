﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework10
{
    public class Edge<V,T> where V: INode<T>
    {
        public int From { get; set; }
        public int To { get; set; }

        public int Weight { get; set; }

        public Edge(int from, int to, int weight)
        {
            From = from;
            To = to;
            Weight = weight;
        }

        public Edge(V from, V to, int weight)
        {
            From = from.Id;
            To = to.Id;
            Weight = weight;
        }

        public override string ToString()
        {
            return $"From {From} to {To}, Weight: {Weight}";
        }
    }
}
