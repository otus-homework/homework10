﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework10
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; }
        int EdgesWeightSum { get; set; }
        bool IsUnvisited { get; set; }
        public INode<T> PreviousVertex { get; set; }
    }
}
